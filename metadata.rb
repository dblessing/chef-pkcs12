name             'pkcs12'
maintainer       'Drew A. Blessing'
maintainer_email 'cookbooks@blessing.io'
license          'Apache 2.0'
description      'Create PKCS12 certificates'
long_description 'Create PKCS12 certificates on the fly'
version          '0.1.0'

depends 'poise', '~> 2.1'
