require 'poise'
require 'openssl'
require 'chef/resource'
require 'chef/provider'

module Pkcs12
  class Resource < Chef::Resource
    include Poise

    provides :pkcs12
    actions :create

    attribute :name, kind_of: String
    attribute :path, kind_of: String, required: true
    attribute :certificate_path, kind_of: String
    attribute :certificate, kind_of: String
    attribute :key_path, kind_of: String
    attribute :key, kind_of: String
    attribute :cacert_paths, kind_of: Array
    attribute :cacerts, kind_of: Array
    attribute :passphrase, kind_of: String
    attribute :owner, kind_of: String, default: 'root'
    attribute :group, kind_of: String, default: 'root'
    attribute :mode, kind_of: String, default: '0640'
  end

  class Provider < Chef::Provider
    include Poise

    provides :pkcs12

    def action_create
      notifying_block do
        pkcs12 =
          OpenSSL::PKCS12.create(
            new_resource.passphrase, new_resource.name, rsa_key, x509_cert, x509_cacerts
          )

        file new_resource.path do
          owner new_resource.owner
          group new_resource.group
          mode new_resource.mode
          content pkcs12.to_der
        end
      end
    end

    def rsa_key
      if new_resource.key && new_resource.key_path
        Chef::Log.warn('Specified both `key` and `key_path`. `key` takes precedence.')
      end
      return OpenSSL::PKey::RSA.new(new_resource.key) if new_resource.key
      OpenSSL::PKey::RSA.new(::File.read(new_resource.key_path))
    end

    def x509_cert
      if new_resource.certificate && new_resource.certificate_path
        Chef::Log.warn('Specified both `certificate` and `certificate_path`. `certificate` takes precedence.')
      end
      return x509_certificate(new_resource.certificate) if new_resource.certificate
      x509_certificate(::File.read(new_resource.certificate_path))
    end

    def x509_cacerts
      if new_resource.cacerts && new_resource.cacert_paths
        Chef::Log.warn('Specified both `cacerts` and `cacert_paths`. `cacerts` takes precedence.')
      end
      cacerts = []
      if new_resource.cacerts
        new_resource.cacerts.each do |cacert|
          cacerts << x509_certificate(cacert)
        end
        return cacerts
      end
      new_resource.cacert_paths.each do |cacert_path|
        cacerts << x509_certificate(::File.read(cacert_path))
      end
      cacerts
    end

    def x509_certificate(raw)
      OpenSSL::X509::Certificate.new(raw)
    end
  end
end
