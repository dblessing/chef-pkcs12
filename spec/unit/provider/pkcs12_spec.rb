require 'spec_helper'

describe Pkcs12::Provider do
  let(:resource) do
    resource = Pkcs12::Resource.new('fake')
    resource.path('/etc/pki/tls/my_keystore.pkcs12')
    resource
  end

  let(:node) { double('Chef::Node') }
  let(:events) { double('Chef::Events').as_null_object }  # mock all the methods
  let(:run_context) { double('Chef::RunContext', :node => node, :events => events) }

  let(:provider) do
    described_class.new(resource, run_context)
  end

  it { expect(provider).to be_a_kind_of(described_class) }

  it { expect(provider.new_resource.path).to eql('/etc/pki/tls/my_keystore.pkcs12') }
end
