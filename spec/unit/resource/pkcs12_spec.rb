require 'spec_helper'

describe Pkcs12::Resource do
  before do
    @resource = Pkcs12::Resource.new('fake')
  end

  it { expect(@resource.name).to eql('fake') }
end
